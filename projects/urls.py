from django.urls import path
from projects.views import (
    project_list,
    project_detail,
    create_project,
)

urlpatterns = [
    path("create/", create_project, name="create_project"),
    path("<int:id>/", project_detail, name="show_project"),
    path("", project_list, name="list_projects"),
]
